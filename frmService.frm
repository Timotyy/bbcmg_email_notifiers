VERSION 5.00
Begin VB.Form frmService 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "BBCMG Mail Notifier"
   ClientHeight    =   3435
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5745
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3435
   ScaleWidth      =   5745
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   495
      Left            =   120
      TabIndex        =   5
      Top             =   2760
      Width           =   1215
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      Default         =   -1  'True
      Height          =   495
      Left            =   3000
      TabIndex        =   2
      Top             =   2760
      Width           =   1215
   End
   Begin VB.CommandButton cmdStop 
      Cancel          =   -1  'True
      Caption         =   "Stop"
      Enabled         =   0   'False
      Height          =   495
      Left            =   4320
      TabIndex        =   1
      Top             =   2760
      Width           =   1215
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   5000
      Left            =   120
      Top             =   2280
   End
   Begin VB.Label lblWaiting 
      Caption         =   "The eMail Notifier is Waiting"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   495
      Left            =   120
      TabIndex        =   4
      Top             =   1680
      Width           =   5415
   End
   Begin VB.Label lblRunning 
      Caption         =   "The eMail Notifier is Running"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF8080&
      Height          =   495
      Left            =   120
      TabIndex        =   3
      Top             =   1200
      Visible         =   0   'False
      Width           =   5415
   End
   Begin VB.Label lbl 
      Caption         =   "BBC Motion Gallery Mail Notifier"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   5175
   End
End
Attribute VB_Name = "frmService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdClose_Click()
    End
End Sub

Private Sub cmdStart_Click()

    Timer1.Enabled = True
    cmdStop.Enabled = True
    cmdStart.Enabled = False
    cmdClose.Enabled = False
    
End Sub

Private Sub cmdStop_Click()
    
    Timer1.Enabled = False
    cmdStart.Enabled = True
    cmdStop.Enabled = False
    cmdClose.Enabled = True
    
End Sub

Private Sub Timer1_Timer()

    lblRunning.Visible = True
    lblWaiting.Visible = False
    
    Initialise_cnn
    
    If GetMailJobs() > 1 Then
        '
    End If
    
    lblWaiting.Visible = True
    lblRunning.Visible = False
    
End Sub
