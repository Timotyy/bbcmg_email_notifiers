Attribute VB_Name = "modMailNotify"
Option Explicit

Public g_strSMTPServer
Public g_strSMTPServerBBC
Const g_strSMTPUserName = ""
Const g_strSMTPPassword = ""
Const g_strEmailFooter = "BBC Worldwide Motion Gallery - Portal at RRMedia"
Public g_strUserEmailAddress As String
Public g_strUserEmailName As String
Public g_strConnection As String
Public cnn As ADODB.Connection
Public bTestMode As Boolean
Public g_strPathToFailedCRM As String
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public Sub Initialise_cnn()

g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=lon-sql-05.office.refine-group.com;Failover_Partner=lon-sql-06.office.refine-group.com;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
'g_strConnection = "DRIVER=SQL Server;SERVER=jca-sql-1.jcatv.co.uk;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
'g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=MX1TEST-sql-3;Failover_Partner=MX1TEST-sql-4;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"

Set cnn = New ADODB.Connection

'set to true to send to internal test mails
bTestMode = False

g_strSMTPServer = GetData("setting", "value", "name", "SMTPServer")
g_strSMTPServerBBC = GetData("setting", "value", "name", "SMTPServerBBC")
       
End Sub

Private Function WriteTextFile(fName As String, _
  sText As String) As Boolean

  Dim FSO As New FileSystemObject
  Dim FSTR As Scripting.TextStream
  
  On Error Resume Next
  
  Set FSTR = FSO.OpenTextFile(fName, ForWriting, _
    Not FSO.FileExists(fName))
    
  FSTR.Write sText
  
  WriteTextFile = True
  
  FSTR.Close
  
  If Err.Number Then WriteTextFile = False
  On Error GoTo 0
  
  Set FSTR = Nothing
  Set FSO = Nothing
End Function

Public Function GetMailJobs() As Integer

    On Error GoTo GetData_Err

    Dim l_strSQL As String
    Dim l_strSQLUpdate As String
    Dim l_strSentSQL As String
    Dim l_strItemSQL As String

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_rstItemData As New ADODB.Recordset
    Dim l_rstCirculation As ADODB.Recordset
    
    Dim l_con As New ADODB.Connection

    'queue items and message templates
    Dim Notification_Queue_Id As Long
    'templates
    Dim Notification_Id As Long
    Dim Notification_Name As String
    Dim Notification_Status_Reached As Long
    Dim Notification_Type As String
    Dim Notification_Title As String
    Dim Notification_Body As String
    Dim Notification_To As String
    Dim Notification_CC As String
    Dim Notification_BCC As String
    'ser data
    Dim Preview_Order_Id As Long
    Dim Clip_Order_Id As Long
    Dim i As Long
    
    Dim thisTo As String
    Dim thisToSecond As String
    Dim thisCC As String
    Dim thisBCC As String
    Dim thisTitle As String
    Dim thisBody As String
    Dim thisTable As String
    Dim thisFooter As String
    Dim thisDateReceived As String
    Dim thisCustomerEmail As String
    Dim thisClipID As String
    
    Dim thisDeliveryMethod As String
    Dim thisFormat As String
    Dim thisDuration As String
    
    Dim thisDeliveryKey As String
    Dim thisAsperaCredentials As String
    Dim thisAsperaPassword As String
    Dim thisAsperaURL As String
    Dim thisSubmitter As String
    Dim thisPreviewOrderId As String
    Dim thisLongClipCount As Variant
    
    Dim thisKey As String
    
    Dim thisStatus As String
    Dim thisNextStatus As String
    
    Dim thisFrom As String
    Dim thisFTP As String
    
    thisFrom = "noreply1@visualdatamedia.com"
    
    Dim thisLogo As String
    Dim sUTF8 As String
    
    thisLogo = "<img src='https://cd-admin.visualdatamedia.com/exchangesignatures/Getty_Images_BBC_Motion_Gallery.jpg'>"
    thisAsperaURL = "https://bbcmg.visualdatamedia.com"
'    thisAsperaURL = "https://bbcmg-uat.visualdatamedia.com:8443"
    
    Dim thisRequestNumber As String
    Dim thisPreviewRequestNumber As String
    Dim thisCustomerName As String
    Dim thisSecureLink As String
    Dim thisRow As String
    Dim thisPriority As String
    Dim thisTotalLength As String
    Dim thisVideoFormat As String
    Dim thisMasterSLA As String
    Dim thisCSVText As String
    Dim thisCSVFile As String
    Dim thisAttachment As String
    Dim thisAttachment2 As String
    Dim thisPreviewFTP As String
    Dim thisBarcode As String
    
    Dim unames() As String
    Dim thisName As String
    
    Dim Count As Long
    
    l_con.Open g_strConnection
    l_strSQL = "SELECT TOP 1 * from BBCMG_Notification_Queue where Notification_Status=0 AND error_notified IS NULL"
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly
    
    Count = 0
    
    If Not IsNull(l_rstGetData) Then
        Do While Not l_rstGetData.EOF
            
            Preview_Order_Id = 0
            Clip_Order_Id = 0
            Count = Count + 1
            
            With l_rstGetData
                For i = 0 To .Fields.Count - 1
                    If Not IsNull(.Fields(i).Value) Then
                        If .Fields(i).Name = "Notification_Queue_Id" Then
                            'its a master clip order
                            Notification_Queue_Id = CLng(.Fields(i).Value)
                            If Notification_Queue_Id > 0 Then
                                
                            End If
                        End If
                        If .Fields(i).Name = "Notification_Id" Then
                            'its a master clip order
                            Notification_Id = CLng(.Fields(i).Value)
                            If Notification_Id > 0 Then
                                Notification_Name = GetData("BBCMG_Notification", "Notification_Name", "Notification_Id", Notification_Id)
                                If Notification_Name <> "" Then
                                End If
                                Notification_Title = GetData("BBCMG_Notification", "Notification_Title", "Notification_Id", Notification_Id)
                                If Notification_Name <> "" Then
                                End If
                                Notification_Body = GetData("BBCMG_Notification", "Notification_Body", "Notification_Id", Notification_Id)
                                If Notification_Body <> "" Then
                                End If
                                Notification_To = GetData("BBCMG_Notification", "Notification_To", "Notification_Id", Notification_Id)
                                If Notification_To <> "" Then
                                End If
                                Notification_CC = GetData("BBCMG_Notification", "Notification_CC", "Notification_Id", Notification_Id)
                                If Notification_CC <> "" Then
                                End If
                                Notification_BCC = GetData("BBCMG_Notification", "Notification_BCC", "Notification_Id", Notification_Id)
                                If Notification_BCC <> "" Then
                                End If
                            End If
                        End If
                        If .Fields(i).Name = "Preview_Order_Id" Then
                            'its a preview order
                            Preview_Order_Id = CLng(.Fields(i).Value)
                            If Preview_Order_Id > 0 Then
                                
                            End If
                        End If
                        If .Fields(i).Name = "Master_Order_Id" Then
                            'its a master clip order
                            Clip_Order_Id = CLng(.Fields(i).Value)
                            If Clip_Order_Id > 0 Then
                                
                            End If
                        End If
                        If .Fields(i).Name = "ModifiedOn" Then
                            'last column so action the message
                            'MsgBox "got Queued Item " + Notification_Name
                        End If
                    End If
                Next
                .MoveNext
            End With

            'get notification template
            Select Case Notification_Id
            
            Case 1  'order receieved
                If Preview_Order_Id > 0 Then
                    Sleep 5000
                    thisFrom = "noreply@visualdatamedia.com"
                    
                    thisRequestNumber = GetData("BBCMG_Preview_Order", "Request_Number", "Preview_Order_Id", Preview_Order_Id)
                    thisCustomerName = GetData("BBCMG_Preview_Order", "Full_Name_Customer", "Preview_Order_Id", Preview_Order_Id)
                    If thisCustomerName = "" Then thisCustomerName = GetData("BBCMG_Preview_Order", "E_Mail_1_Customer", "Preview_Order_Id", Preview_Order_Id)
                    thisDateReceived = GetData("BBCMG_Preview_Order", "CreatedOn", "Preview_Order_Id", Preview_Order_Id)
                    
                    'build message
                    thisSubmitter = GetData("BBCMG_Preview_Order", "Owner", "Preview_Order_Id", Preview_Order_Id)
                    thisTo = GetData("BBCMG_Sales_Lookup", "eMail", "Alt_Name", thisSubmitter)
                    If thisTo = "" Then thisTo = GetData("BBCMG_Sales_Lookup", "eMail", "Name", thisSubmitter)
                    thisCC = GetData("BBCMG_Preview_Order", "Created_By_Email", "Preview_Order_Id", Preview_Order_Id)
                    thisBCC = "thart@visualdatamedia.com,bbcmg-VIE@visualdatamedia.com"
                    
                    'test overrides
                    If bTestMode Then
                        thisBCC = ""
                        thisCC = "rrmedia.test2@gmail.com"
                        thisTo = "rrmedia.test1@gmail.com"
                    End If
                    
                    thisTitle = Replace(Notification_Title, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    thisBody = Replace(Notification_Body, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    
                    If InStr(thisCustomerName, ",") > 0 Then
                        unames = Split(thisCustomerName, ",")
                        thisName = unames(1)
                    Else
                        thisName = thisCustomerName
                    End If
                    
                    
                    thisBody = Replace(thisBody, "@@CUSTOMERNAME", thisCustomerName, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@POID", CStr(Preview_Order_Id), 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@DATERECEIVED", thisDateReceived, 1, -1, vbTextCompare)
                    
                    l_strItemSQL = "SELECT Program_ID, Program_Title, Tape_Number, Source, BBCMG_SLA.SLA_Title, BBCMG_Order_Status.Order_Status " + vbCrLf
                    l_strItemSQL = l_strItemSQL + " From BBCMG_Preview_Order " + vbCrLf
                    l_strItemSQL = l_strItemSQL + " INNER JOIN BBCMG_Order_Status on BBCMG_Order_Status.Order_Status_Id=BBCMG_Preview_Order.Order_Status_Id " + vbCrLf
                    l_strItemSQL = l_strItemSQL + " INNER JOIN BBCMG_SLA on BBCMG_SLA.SLA_Id=BBCMG_Preview_Order.Priority " + vbCrLf
                    l_strItemSQL = l_strItemSQL + " WHERE RTRIM(BBCMG_Preview_Order.Request_Number)='" + Trim(thisRequestNumber) + "'"
                    
                    l_rstItemData.Open l_strItemSQL, l_con, adOpenStatic, adLockReadOnly
                    
                    thisTable = "<table width='100%'><tr><td><strong>Program ID</strong></td>"
                    thisTable = thisTable + "<td><strong>Program Title</strong></td>"
                    thisTable = thisTable + "<td><strong>Tape Number</strong></td>"
                    thisTable = thisTable + "<td><strong>Source</strong></td>"
                    thisTable = thisTable + "<td><strong>Priority</strong></td>"
                    thisTable = thisTable + "<td><strong>Status</strong></td></tr>"
                    
                    On Local Error Resume Next
                    
                    If Not IsNull(l_rstItemData) Then
                        l_rstItemData.MoveFirst
                        Do While Not l_rstItemData.EOF
                            
                           thisRow = "<tr>"
                           For i = 0 To l_rstItemData.Fields.Count - 1
                               If Not IsNull(l_rstItemData.Fields(i).Value) Then
                                   thisRow = thisRow + "<td>" + CStr(l_rstItemData.Fields(i).Value) + "</td>"
                               Else
                                   thisRow = thisRow + "<td>&nbsp;</td>"
                               End If
                           Next
                           thisRow = thisRow + "</tr>"
                           thisTable = thisTable + thisRow
                           l_rstItemData.MoveNext
                           
                        Loop
                    End If
                    
                    thisTable = thisTable + "</table>"
                    
                    thisBody = Replace(thisBody, "@@POTABLE", thisTable, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@BBCMGIMAGE", thisLogo, 1, -1, vbTextCompare)
                    
                    If SendMail(thisFrom, thisTo, thisCC, thisBCC, thisTitle, thisBody, l_rstGetData("error_notified")) Then
                        l_strSentSQL = "INSERT INTO BBCMG_Notification_Sent(Notification_Id,Preview_Order_Id,Sent_To,CC_To, BCC_To)"
                        l_strSentSQL = l_strSentSQL + " VALUES(" + CStr(Notification_Id) + "," + CStr(Preview_Order_Id) + ",'" + thisTo + "','" + thisCC + "','" + thisBCC + "')"
                        l_con.Execute l_strSentSQL
                        
                        If SetData("BBCMG_Notification_Queue", "Notification_Status", "Notification_Queue_Id", Notification_Queue_Id, 1) Then
                        End If
                    Else
                        If SetData("BBCMG_Notification_Queue", "error_notified", "Notification_Queue_Id", Notification_Queue_Id, Format(Now, "YYYY-MM-DD HH:NN:SS")) Then
                        End If
                    End If
                    
                    l_rstItemData.Close
                    
                End If
                
            Case 2  'preview portal ready
                If Preview_Order_Id > 0 Then
                
                    thisFrom = "BBC.MotionGallerySales@gettyimages.com"
'                    thisFrom = "noreply@visualdatamedia.com"
                    
                    thisRequestNumber = GetData("BBCMG_Preview_Order", "Request_Number", "Preview_Order_Id", Preview_Order_Id)
                    thisCustomerName = GetData("BBCMG_Preview_Order", "Full_Name_Customer", "Preview_Order_Id", Preview_Order_Id)
                    
                    If InStr(thisCustomerName, ",") > 0 Then
                        unames = Split(thisCustomerName, ",")
                        thisName = unames(1)
                    Else
                        thisName = thisCustomerName
                    End If
                    
                    
                    thisTo = ""
                    'thisTo = GetData("BBCMG_Preview_Order", "E_mail_1_Customer", "Preview_Order_Id", Preview_Order_Id)
                    
                    If GetData("BBCMG_Preview_Order", "Primary_Delivery_Email_Notification", "Preview_Order_Id", Preview_Order_Id) <> "" Then
                        If thisTo = "" Then
                            thisTo = thisTo + GetData("BBCMG_Preview_Order", "Primary_Delivery_Email_Notification", "Preview_Order_Id", Preview_Order_Id)
                        Else
                            thisTo = thisTo + "," + GetData("BBCMG_Preview_Order", "Primary_Delivery_Email_Notification", "Preview_Order_Id", Preview_Order_Id)
                        End If
                    End If
                    If GetData("BBCMG_Preview_Order", "Secondary_Delivery_Email_Notification", "Preview_Order_Id", Preview_Order_Id) <> "" Then
                        thisTo = thisTo + "," + GetData("BBCMG_Preview_Order", "Secondary_Delivery_Email_Notification", "Preview_Order_Id", Preview_Order_Id)
                    End If
                    
                    'cc sales contact
                    thisCC = GetData("BBCMG_Preview_Order", "Created_By_Email", "Preview_Order_Id", Preview_Order_Id)
                    thisBCC = "WWMG.Operations@bbc.com,sales-ops.support@gettyimages.com,thart@visualdatamedia.com,bbcmg-VIE@visualdatamedia.com"
                    
                    'test overrides
                    If bTestMode Then
                        thisBCC = ""
                        thisCC = "rrmedia.test2@gmail.com"
                        thisTo = "rrmedia.test1@gmail.com"
                    End If
                    
                    'thisPreviewFTP construction
                    thisKey = GetData("BBCMG_Request_Key", "Request_Key", "Request_Number", thisRequestNumber)
                    'thisPreviewFTP = Mid(thisKey, 2, 13)
                    thisPreviewFTP = "Alternatively, the preview files for this order can be downloaded via ftp from ftp://ftp-vie.visualdatamedia.com using the userID " _
                    & LCase(Mid(thisKey, 2, 13)) & " and password " & LCase(Mid(thisKey, 2, 13))
                    
                    
                    'build message
                    thisTitle = Replace(Notification_Title, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    thisBody = Replace(Notification_Body, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@CUSTOMERNAME", thisName, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@POID", CStr(Preview_Order_Id), 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@FTPLINK", thisPreviewFTP, 1, -1, vbTextCompare)

                    thisSecureLink = "<a href='https://bbcportal.gettyimages.com/default.php?id=" + thisKey + "'>Secure Link</a>"
                    thisSecureLink = Replace(thisSecureLink, "{", "", 1, -1, vbTextCompare)
                    thisSecureLink = Replace(thisSecureLink, "}", "", 1, -1, vbTextCompare)
                    
                    thisBody = Replace(thisBody, "@@SECURELINK", thisSecureLink, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@GETTYLOGO", thisLogo, 1, -1, vbTextCompare)
                    
                    'manage user guide attachment
                    thisAttachment = ""
                    thisAttachment = Replace(App.Path, "C:\Users\Tim.JCADOMAIN\source\repos\BBCMG_Email_Notifiers", "\\lat-vb6-dev-1") + "\attachments\BBCMG-GettyImages_Preview_Portal_V1.pdf"
                    thisAttachment2 = ""
                    thisAttachment2 = Replace(App.Path, "C:\Users\Tim.JCADOMAIN\source\repos\BBCMG_Email_Notifiers", "\\lat-vb6-dev-1") + "\attachments\MOTION_GALLERY_INFO_GUIDE.pdf"
                    
                    If SendMailFromBBC(thisFrom, thisTo, thisCC, thisBCC, thisTitle, thisBody, l_rstGetData("error_notified"), thisAttachment, thisAttachment2) Then
'                    If SendMail(thisFrom, thisTo, thisCC, thisBCC, thisTitle, thisBody, l_rstGetData("error_notified"), thisAttachment) Then
                        l_strSentSQL = "INSERT INTO BBCMG_Notification_Sent(Notification_Id,Preview_Order_Id,Sent_To,CC_To, BCC_To)"
                        l_strSentSQL = l_strSentSQL + " VALUES(" + CStr(Notification_Id) + "," + CStr(Preview_Order_Id) + ",'" + thisTo + "','" + thisCC + "','" + thisBCC + "')"
                        l_con.Execute l_strSentSQL
                    
                        If SetData("BBCMG_Notification_Queue", "Notification_Status", "Notification_Queue_Id", Notification_Queue_Id, 1) Then
                        End If
                    Else
                        If SetData("BBCMG_Notification_Queue", "error_notified", "Notification_Queue_Id", Notification_Queue_Id, Format(Now, "YYYY-MM-DD HH:NN:SS")) Then
                        End If
                    End If
                    
                End If
                
            Case 4  'clip selection for quoting
                If Clip_Order_Id > 0 Then
                
                    thisFrom = "noreply@visualdatamedia.com"
                    
                    thisRequestNumber = GetData("BBCMG_Clip_Order", "Request_Number", "Clip_Order_Id", Clip_Order_Id)
                    thisPreviewRequestNumber = GetData("BBCMG_Clip_Order", "Preview_Request_Number", "Clip_Order_Id", Clip_Order_Id)
                    'thisCustomerName = GetData("BBCMG_Clip_Order", "Full_Name_Customer", "Clip_Order_Id", Clip_Order_Id)
                    thisDateReceived = GetData("BBCMG_Clip_Order", "CreatedOn", "Clip_Order_Id", Clip_Order_Id)
                    thisDeliveryMethod = GetData("BBCMG_Clip_Order", "Delivery_Method_BBC", "Clip_Order_Id", Clip_Order_Id)
                    thisFormat = GetData("BBCMG_Clip_Order", "Video_Format_Required", "Clip_Order_Id", Clip_Order_Id)
                    thisDuration = GetData("vw_BBCMG_Clip_Order_Summary", "Duration", "Request_Number", thisRequestNumber)
                    thisLongClipCount = GetDataSQL("SELECT count(*) as 'Count' FROM BBCMG_Clip_Order Inner Join BBCMG_Order_Status on BBCMG_Order_Status.Order_Status_Id=BBCMG_Clip_Order.Order_Status_Id WHERE Request_Number='" & thisPreviewRequestNumber & "' AND BBCMG_Order_Status.Order_Status='Master Release Pending' AND Clip_Duration>300")
                    If Not IsNull(GetData("BBCMG_Clip_Order", "Preview_Order_id", "Clip_Order_Id", Clip_Order_Id)) Then
                        
                        Preview_Order_Id = GetData("BBCMG_Clip_Order", "Preview_Order_id", "Clip_Order_Id", Clip_Order_Id)
                    End If
                    
                    '[E_mail_1_Customer]
                    thisCustomerEmail = GetData("BBCMG_Preview_Order", "E_mail_1_Customer", "Preview_Order_Id", Preview_Order_Id)
                    
                    'build message
                    thisTo = GetData("BBCMG_Preview_Order", "Created_By_Email", "Preview_Order_Id", Preview_Order_Id)
                    thisBCC = "WWMG.Operations@bbc.com,sales-ops.support@gettyimages.com,thart@visualdatamedia.com,bbcmg-VIE@visualdatamedia.com"
                    
                    'test overrides
                    If bTestMode Then
                        thisBCC = ""
                        thisCC = "rrmedia.test2@gmail.com"
                        thisTo = "rrmedia.test1@gmail.com"
                    End If
                                      
                    'MsgBox "got Queued Item " + Notification_Name + " POID - " + CStr(Preview_Order_Id)
                    thisTitle = Replace(Notification_Title, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    thisBody = Replace(Notification_Body, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    'thisBody = Replace(thisBody, "@@CUSTOMERNAME", thisCustomerName, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@POID", CStr(Preview_Order_Id), 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@DATERECEIVED", thisDateReceived, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@VIDEOFORMAT", thisFormat, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@DELIVERYMETHOD", thisDeliveryMethod, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@CUSTOMEREMAIL", thisCustomerEmail, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@TOTALLENGTH", thisDuration, 1, -1, vbTextCompare)
                    If Val(thisLongClipCount) > 0 Then
                        thisBody = Replace(thisBody, "@@LONGCLIPS", "<p><strong>Please note - There are " & thisLongClipCount & " clips in this order that have durations longer than 5 minutes.</strong></p>", 1, -1, vbTextCompare)
                    Else
                        thisBody = Replace(thisBody, "@@LONGCLIPS", "", 1, -1, vbTextCompare)
                    End If

                    l_strItemSQL = "SELECT TX_Date, Timecode_In, Timecode_Out, Program_Id , Program_Title, Shot_Info From BBCMG_Clip_Order "
                    l_strItemSQL = l_strItemSQL + "WHERE RTRIM(BBCMG_Clip_Order.Request_Number)='" + Trim(thisRequestNumber) + "'"
                        
                    l_rstItemData.Open l_strItemSQL, l_con, adOpenStatic, adLockReadOnly
                    
                    thisTable = "<table><tr><td><strong>Clip ID</strong></td>"
                    thisTable = thisTable + "<td><strong>TX Date</strong></td>"
                    thisTable = thisTable + "<td><strong>Time In</strong></td>"
                    thisTable = thisTable + "<td><strong>Time Out</strong></td>"
                    thisTable = thisTable + "<td><strong>BBC Programme ID</strong></td>"
                    thisTable = thisTable + "<td><strong>Programme Title</strong></td>"
                    thisTable = thisTable + "<td><strong>Shot Description \&amp; Context of Use</strong></td></tr>"
                    
                    thisCSVText = ""
                    
                    thisCSVText = "Request Number,Clip ID, Program Title, Shot Info, Program ID, TX Date, Timecode In, Timecode Out" + vbCrLf
                    
                    On Local Error Resume Next
                    
                    If Not IsNull(l_rstItemData) Then
                        l_rstItemData.MoveFirst
                        Do While Not l_rstItemData.EOF
                            thisCSVText = thisCSVText + """" + thisRequestNumber + ""","
                            thisCSVText = thisCSVText + """" + GetDataSQL("SELECT Clip_ID FROM BBCMG_Preview_Order WHERE Request_Number = '" & thisPreviewRequestNumber & "' AND Program_ID = '" & l_rstItemData.Fields("Program_ID") & "'") + ""","
                            thisCSVText = thisCSVText + """" + l_rstItemData.Fields("Program_Title") + ""","
                            thisCSVText = thisCSVText + """" + Replace(Replace(Replace(Replace(Replace(l_rstItemData.Fields("Shot_Info"), "&#x27;", "'"), "&gt;", ">"), "&lt;", "<"), "&quot;", """"), "&#x2F;", "/") + ""","
                            thisCSVText = thisCSVText + """" + l_rstItemData.Fields("Program_ID") + ""","
                            thisCSVText = thisCSVText + """" + l_rstItemData.Fields("TX_Date") + ""","
                            thisCSVText = thisCSVText + """" + l_rstItemData.Fields("Timecode_In") + ""","
                            thisCSVText = thisCSVText + """" + l_rstItemData.Fields("TImecode_Out") + """" + vbCrLf
                            thisRow = "<tr>"
                            If Not IsNull(GetDataSQL("SELECT Clip_ID FROM BBCMG_Preview_Order WHERE Request_Number = '" & thisPreviewRequestNumber & "' AND Program_ID = '" & l_rstItemData.Fields("Program_ID") & "'")) Then
                                thisRow = thisRow + "<td>" + GetDataSQL("SELECT Clip_ID FROM BBCMG_Preview_Order WHERE Request_Number = '" & thisPreviewRequestNumber & "' AND Program_ID = '" & l_rstItemData.Fields("Program_ID") & "'") + "</td>"
                            Else
                                thisRow = thisRow + "<td>&nbsp;</td>"
                            End If
                            If Not IsNull(l_rstItemData.Fields("TX_Date").Value) Then
                                thisRow = thisRow + "<td>" + CStr(l_rstItemData.Fields("TX_Date").Value) + "</td>"
                            Else
                                thisRow = thisRow + "<td>&nbsp;</td>"
                            End If
                            If Not IsNull(l_rstItemData.Fields("Timecode_in").Value) Then
                                thisRow = thisRow + "<td>" + CStr(l_rstItemData.Fields("Timecode_in").Value) + "</td>"
                            Else
                                thisRow = thisRow + "<td>&nbsp;</td>"
                            End If
                            If Not IsNull(l_rstItemData.Fields("Timecode_out").Value) Then
                                thisRow = thisRow + "<td>" + CStr(l_rstItemData.Fields("Timecode_out").Value) + "</td>"
                            Else
                                thisRow = thisRow + "<td>&nbsp;</td>"
                            End If
                            If Not IsNull(l_rstItemData.Fields("Program_ID").Value) Then
                                thisRow = thisRow + "<td>" + CStr(l_rstItemData.Fields("Program_ID").Value) + "</td>"
                            Else
                                thisRow = thisRow + "<td>&nbsp;</td>"
                            End If
                            If Not IsNull(l_rstItemData.Fields("Program_Title").Value) Then
                                thisRow = thisRow + "<td>" + CStr(l_rstItemData.Fields("Program_Title").Value) + "</td>"
                            Else
                                thisRow = thisRow + "<td>&nbsp;</td>"
                            End If
                            If Not IsNull(l_rstItemData.Fields("Shot_Info").Value) Then
                                thisRow = thisRow + "<td>" + CStr(l_rstItemData.Fields("Shot_Info").Value) + "</td>"
                            Else
                                thisRow = thisRow + "<td>&nbsp;</td>"
                            End If
                            thisRow = thisRow + "</tr>"
                            thisTable = thisTable + thisRow
                            l_rstItemData.MoveNext
                        Loop
                    End If
                    
                    'manage attachment
                    thisAttachment = ""
                    If thisCSVText <> "" Then
                        thisCSVFile = App.Path + "\attachments\" + thisRequestNumber + ".csv"
                        If WriteTextFile(thisCSVFile, thisCSVText) Then
                            'file was created - so att it as ann attachment
                            thisAttachment = thisCSVFile
                        End If
                    End If
                    
                    thisTable = thisTable + "</table>"
                    
                    thisBody = Replace(thisBody, "@@POTABLE", thisTable, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@GETTYLOGO", thisLogo, 1, -1, vbTextCompare)
                                        
                    If SendMail(thisFrom, thisTo, thisCC, thisBCC, thisTitle, thisBody, l_rstGetData("error_notified"), thisAttachment) Then
                        If Preview_Order_Id > 0 Then
                            l_strSentSQL = "INSERT INTO BBCMG_Notification_Sent(Notification_Id,Master_Order_Id,Preview_Order_id,Sent_To,CC_To, BCC_To)"
                            l_strSentSQL = l_strSentSQL + " VALUES(" + CStr(Notification_Id) + "," + CStr(Clip_Order_Id) + "," + CStr(Preview_Order_Id) + ",'" + thisTo + "','" + thisCC + "','" + thisBCC + "')"
                        Else
                            l_strSentSQL = "INSERT INTO BBCMG_Notification_Sent(Notification_Id,Master_Order_Id,Sent_To,CC_To, BCC_To)"
                            l_strSentSQL = l_strSentSQL + " VALUES(" + CStr(Notification_Id) + "," + CStr(Clip_Order_Id) + ",'" + thisTo + "','" + thisCC + "','" + thisBCC + "')"
                        End If
                        l_con.Execute l_strSentSQL
                        
                        If SetData("BBCMG_Notification_Queue", "Notification_Status", "Notification_Queue_Id", Notification_Queue_Id, 1) Then
                        End If
                    Else
                        If SetData("BBCMG_Notification_Queue", "error_notified", "Notification_Queue_Id", Notification_Queue_Id, Format(Now, "YYYY-MM-DD HH:NN:SS")) Then
                        End If
                    End If
                    
                End If
            
            Case 5
                '   masters released
                If Clip_Order_Id > 0 Then
                
                    thisFrom = "noreply@visualdatamedia.com"
                    
                    thisRequestNumber = GetData("BBCMG_Clip_Order", "Request_Number", "Clip_Order_Id", Clip_Order_Id)
                    thisPreviewRequestNumber = GetData("BBCMG_Clip_Order", "Preview_Request_Number", "Clip_Order_Id", Clip_Order_Id)
                    thisPreviewOrderId = GetData("BBCMG_Clip_Order", "Preview_Order_id", "Clip_Order_Id", Clip_Order_Id)
                    thisCustomerName = GetData("BBCMG_Preview_Order", "Full_Name_Customer", "Preview_Order_Id", thisPreviewOrderId)
                    thisDateReceived = GetData("BBCMG_Clip_Order", "CreatedOn", "Clip_Order_Id", Clip_Order_Id)
                    thisDeliveryMethod = GetData("BBCMG_Clip_Order", "Delivery_Method_BBC", "Clip_Order_Id", Clip_Order_Id)
                    thisFormat = GetData("BBCMG_Clip_Order", "Video_Format_Required", "Clip_Order_Id", Clip_Order_Id)
                    thisDuration = GetData("vw_BBCMG_Clip_Order_Summary", "Duration", "Request_Number", thisRequestNumber)
                    
                    If InStr(thisCustomerName, ",") > 0 Then
                        unames = Split(thisCustomerName, ",")
                        thisName = unames(1)
                    Else
                        thisName = thisCustomerName
                    End If
                    
                    If Not IsNull(GetData("BBCMG_Clip_Order", "Preview_Order_id", "Clip_Order_Id", Clip_Order_Id)) Then
                        Preview_Order_Id = GetData("BBCMG_Clip_Order", "Preview_Order_id", "Clip_Order_Id", Clip_Order_Id)
                    End If
                    
                    'build message
                    thisTo = GetData("BBCMG_Preview_Order", "Created_By_Email", "Preview_Order_Id", Preview_Order_Id)
                    thisSubmitter = GetData("BBCMG_Preview_Order", "Owner", "Preview_Order_Id", Preview_Order_Id)
                    thisCC = GetData("BBCMG_Sales_Lookup", "eMail", "Alt_Name", thisSubmitter)

                    thisBCC = "WWMG.Operations@bbc.com,thart@visualdatamedia.com,bbcmg-VIE@visualdatamedia.com"
                    
                    'test overrides
                    If bTestMode Then
                        thisBCC = ""
                        thisCC = "rrmedia.test2@gmail.com"
                        thisTo = "rrmedia.test1@gmail.com"
                    End If
                    
                    thisTitle = Replace(Notification_Title, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    thisBody = Replace(Notification_Body, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@CLIENTEMAIL", thisCustomerEmail, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@CUSTOMERNAME", thisCustomerName, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@TOTALLENGTH", thisDuration, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@VIDEOFORMAT", thisFormat, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@MASTERSLA", thisMasterSLA, 1, -1, vbTextCompare)
                                        
                    l_strItemSQL = "SELECT BBCMG_Preview_Order.Clip_ID as 'Clip_ID', BBCMG_Clip_Order.TX_Date, BBCMG_Clip_Order.Timecode_In, BBCMG_Clip_Order.Timecode_Out, " + _
                       "BBCMG_Clip_Order.Program_Id , BBCMG_Clip_Order.Program_Title, BBCMG_Clip_Order.Shot_Info, BBCMG_Clip_Order.Rights_Approval_Status " + _
                    "From BBCMG_Clip_Order  " + _
                               "Inner Join  " + _
                    "BBCMG_Preview_Order on " + _
                    "BBCMG_Preview_Order.Preview_Order_Id = BBCMG_Clip_Order.Preview_Order_Id" + vbCrLf
                    
                    l_strItemSQL = l_strItemSQL + " WHERE RTRIM(BBCMG_Clip_Order.Request_Number)='" + Trim(thisRequestNumber) + "' AND BBCMG_Preview_Order.Order_Status_Id NOT IN(1,8)"
                        
                    l_rstItemData.Open l_strItemSQL, l_con, adOpenStatic, adLockReadOnly
                    
                    thisTable = "<table><tr><td><strong>Clip ID</strong></td>"
                    thisTable = thisTable + "<td><strong>TX Date</strong></td>"
                    thisTable = thisTable + "<td><strong>Time In</strong></td>"
                    thisTable = thisTable + "<td><strong>Time Out</strong></td>"
                    thisTable = thisTable + "<td><strong>BBC Programme ID</strong></td>"
                    thisTable = thisTable + "<td><strong>Programme Title</strong></td>"
                    thisTable = thisTable + "<td><strong>Shot Description &amp; Context of Use</strong></td></tr>"
                    
                    On Local Error Resume Next
                    
                    If Not IsNull(l_rstItemData) Then
                        l_rstItemData.MoveFirst
                        Do While Not l_rstItemData.EOF
                            If Val(l_rstItemData("Rights_Approval_Status")) = Val(GetData("BBCMG_Rights_Status", "Rights_Status_Id", "Rights_Status", "Rights Approved")) Then
                                thisRow = "<tr>"
                                For i = 0 To l_rstItemData.Fields.Count - 1
                                    If l_rstItemData.Fields(i).Name = "Clip_ID" Then
                                        thisClipID = GetData("BBCMG_Preview_Order", "Clip_ID", "Preview_Order_Id", Preview_Order_Id)
                                        If Not IsNull(thisClipID) Then
                                            thisRow = thisRow + "<td>" + CStr(thisClipID) + "</td>"
                                        Else
                                            thisRow = thisRow + "<td>&nbsp;</td>"
                                        End If
                                    Else
                                        If Not IsNull(l_rstItemData.Fields(i).Value) Then
                                            thisRow = thisRow + "<td>" + CStr(l_rstItemData.Fields(i).Value) + "</td>"
                                        Else
                                            thisRow = thisRow + "<td>&nbsp;</td>"
                                        End If
                                    End If
                                Next
                                thisRow = thisRow + "</tr>"
                                thisTable = thisTable + thisRow
                            End If
                            l_rstItemData.MoveNext
                        Loop
                    End If
                    
                    thisTable = thisTable + "</table>"
                    
                    thisBody = Replace(thisBody, "@@CLIPLISTING", thisTable, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@GETTYLOGO", thisLogo, 1, -1, vbTextCompare)
                    
                    If SendMail(thisFrom, thisTo, thisCC, thisBCC, thisTitle, thisBody, l_rstGetData("error_notified")) Then
                    
                        If Preview_Order_Id > 0 Then
                            l_strSentSQL = "INSERT INTO BBCMG_Notification_Sent(Notification_Id,Master_Order_Id,Preview_Order_Id,Sent_To,CC_To, BCC_To)"
                            l_strSentSQL = l_strSentSQL + " VALUES(" + CStr(Notification_Id) + "," + CStr(Clip_Order_Id) + "," + CStr(Preview_Order_Id) + ",'" + thisTo + "','" + thisCC + "','" + thisBCC + "')"
                        Else
                            l_strSentSQL = "INSERT INTO BBCMG_Notification_Sent(Notification_Id,Master_Order_Id,Sent_To,CC_To, BCC_To)"
                            l_strSentSQL = l_strSentSQL + " VALUES(" + CStr(Notification_Id) + "," + CStr(Clip_Order_Id) + ",'" + thisTo + "','" + thisCC + "','" + thisBCC + "')"
                        End If
                        
                        l_con.Execute l_strSentSQL
                        
                        If SetData("BBCMG_Notification_Queue", "Notification_Status", "Notification_Queue_Id", Notification_Queue_Id, 1) Then
                        End If
                    Else
                        If SetData("BBCMG_Notification_Queue", "error_notified", "Notification_Queue_Id", Notification_Queue_Id, Format(Now, "YYYY-MM-DD HH:NN:SS")) Then
                        End If
                    End If
                    
                End If
            
            Case 6
                '   delivery notification
                If Clip_Order_Id > 0 Then
                
                    thisFrom = "BBC.MotionGallerySales@gettyimages.com"
'                    thisFrom = "noreply@visualdatamedia.com"
                    
                    thisRequestNumber = GetData("BBCMG_Clip_Order", "Request_Number", "Clip_Order_Id", Clip_Order_Id)
                    thisPreviewRequestNumber = GetData("BBCMG_Clip_Order", "Preview_Request_Number", "Clip_Order_Id", Clip_Order_Id)
                    thisPreviewOrderId = GetData("BBCMG_Clip_Order", "Preview_Order_id", "Clip_Order_Id", Clip_Order_Id)
                    thisCustomerName = GetData("BBCMG_Preview_Order", "Full_Name_Customer", "Preview_Order_Id", thisPreviewOrderId)
                    thisDateReceived = GetData("BBCMG_Clip_Order", "CreatedOn", "Clip_Order_Id", Clip_Order_Id)
                    thisDeliveryMethod = GetData("BBCMG_Clip_Order", "Delivery_Method_BBC", "Clip_Order_Id", Clip_Order_Id)
                    thisFormat = GetData("BBCMG_Clip_Order", "Video_Format_Required", "Clip_Order_Id", Clip_Order_Id)
                    thisDuration = GetData("vw_BBCMG_Clip_Order_Summary", "Total_Duration", "Request_Number", thisRequestNumber)
                    
                    If Not IsNull(GetData("BBCMG_Clip_Order", "Preview_Order_id", "Clip_Order_Id", Clip_Order_Id)) Then
                        Preview_Order_Id = GetData("BBCMG_Clip_Order", "Preview_Order_id", "Clip_Order_Id", Clip_Order_Id)
                    End If
                    
                    'delivery key
                    thisDeliveryKey = Replace(GetData("BBCMG_Delivery_Key", "Delivery_Key", "Request_Number", thisRequestNumber), "-", "", 1, -1, vbTextCompare)
                    
                    thisAsperaPassword = GetData("portaluser", "password", "fullname", thisRequestNumber)
                    'thisFTP = "<p>Your clips are also available via SFTP by visiting ftp.rrmedia.tv with an FTP client that is capable of SFTP connections using the same credentials as above.</p>"
                    thisFTP = "<p>Your clips are also available via FTP. Using the same credentials above, please go to ftp-vie.visualdatamedia.com and use a suitable client (e.g. FileZilla) to access the content.</p>"
                    
                    
                    If InStr(thisCustomerName, ",") > 0 Then
                        unames = Split(thisCustomerName, ",")
                        thisName = unames(1)
                    Else
                        thisName = thisCustomerName
                    End If
                    
                    
                    thisDeliveryKey = Replace(thisDeliveryKey, "{", "", 1, -1, vbTextCompare)
                    thisDeliveryKey = Replace(thisDeliveryKey, "}", "", 1, -1, vbTextCompare)
                                      
                    thisAsperaCredentials = "<p>Username: " + thisDeliveryKey + "</p>"
                    thisAsperaCredentials = thisAsperaCredentials + "<p>Password: " + thisAsperaPassword + "</p>" + thisFTP
                    
                    '[E_mail_1_Customer]
                    thisCustomerEmail = GetData("BBCMG_Preview_Order", "E_mail_1_Customer", "Preview_Order_Id", thisPreviewOrderId)
                    
                    'build message
                    thisTo = ""
                    'thisTo = GetData("BBCMG_Preview_Order", "E_mail_1_Customer", "Preview_Order_Id", thisPreviewOrderId)
                    If GetData("BBCMG_Clip_Order", "Primary_Delivery_Email_Notification", "Clip_Order_Id", Clip_Order_Id) <> "" Then
                        If thisTo = "" Then
                            thisTo = thisTo + GetData("BBCMG_Clip_Order", "Primary_Delivery_Email_Notification", "Clip_Order_Id", Clip_Order_Id)
                        Else
                            thisTo = thisTo + "," + GetData("BBCMG_Clip_Order", "Primary_Delivery_Email_Notification", "Clip_Order_Id", Clip_Order_Id)
                        End If
                    End If
                    If GetData("BBCMG_Clip_Order", "Secondary_Delivery_Email_Notification", "Clip_Order_Id", Clip_Order_Id) <> "" Then
                        thisTo = thisTo + "," + GetData("BBCMG_Clip_Order", "Secondary_Delivery_Email_Notification", "Clip_Order_Id", Clip_Order_Id)
                    End If
                    
                    thisCC = GetData("BBCMG_Preview_Order", "Created_By_Email", "Preview_Order_Id", thisPreviewOrderId)
                    thisBCC = "WWMG.Operations@bbc.com,sales-ops.support@gettyimages.com,thart@visualdatamedia.com,bbcmg-VIE@visualdatamedia.com"
                    
'                    If thisToSecond <> "" Then
'                        thisTo = thisTo + "," + thisToSecond
'                    End If
'
'                    If thisCustomerEmail <> "" Then
'                        thisTo = thisTo + "," + thisCustomerEmail
'                    End If
                    
                    'test overrides
                    If bTestMode Then
                        thisBCC = ""
                        thisCC = "rrmedia.test2@gmail.com"
                        thisTo = "rrmedia.test1@gmail.com"
                    End If
                    
                    thisTitle = Replace(Notification_Title, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    thisBody = Replace(Notification_Body, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@CUSTOMERNAME", thisName, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@ASPERALINK", thisAsperaURL, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@ASPERACREDENTIALS", thisAsperaCredentials, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@FTPURL", "Not enabled", 1, -1, vbTextCompare)
                                        
                    l_strItemSQL = "SELECT Clip_Reference, Shot_Info " + vbCrLf
                    l_strItemSQL = l_strItemSQL + " From BBCMG_Clip_Order " + vbCrLf
                    l_strItemSQL = l_strItemSQL + " WHERE RTRIM(BBCMG_Clip_Order.Request_Number)='" + Trim(thisRequestNumber) + "' and Order_Status_Id NOT IN(1,8)"
                        
                    l_rstItemData.Open l_strItemSQL, l_con, adOpenStatic, adLockReadOnly
                    
                    thisTable = "<table><tr><td><strong>Filename</strong></td>"
                    thisTable = thisTable + "<td><strong>Shot Description</strong></td></tr>"
                    
                    If Not IsNull(l_rstItemData) Then
                        Do While Not l_rstItemData.EOF
                            thisRow = "<tr>"
                            For i = 0 To l_rstItemData.Fields.Count - 1
                                If l_rstItemData.Fields(i).Name = "Clip_Reference" Then
                                    If Not IsNull(l_rstItemData.Fields(i).Value) Then
                                        thisKey = Trim(CStr(l_rstItemData.Fields(i).Value))
                                        thisKey = Replace(thisKey, "{", "")
                                        thisKey = Replace(thisKey, "}", "")
                                        thisRow = thisRow + "<td>" + Trim(thisKey) + ".mov</td>"
                                    Else
                                        thisRow = thisRow + "<td>&nbsp;</td>"
                                    End If
                                Else
                                    If Not IsNull(l_rstItemData.Fields(i).Value) Then
                                        thisRow = thisRow + "<td>" + CStr(l_rstItemData.Fields(i).Value) + "</td>"
                                    Else
                                        thisRow = thisRow + "<td>&nbsp;</td>"
                                    End If
                                End If
                            Next
                            thisRow = thisRow + "</tr>"
                            thisTable = thisTable + thisRow
                            l_rstItemData.MoveNext
                        Loop
                    End If
                    
                    thisTable = thisTable + "</table>"
                    
                    thisBody = Replace(thisBody, "@@CLIPLISTING", thisTable, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@GETTYLOGO", thisLogo, 1, -1, vbTextCompare)
                    
                    'thisNextStatus = GetData("BBCMG_Order_Status", "Order_Status_Id", "Order_Status", "Available for Delivery")
                    'thisStatus = GetData("BBCMG_Order_Status", "Order_Status_Id", "Order_Status", "Master Generation Complete")
                    
                    'l_strSQLUpdate = "UPDATE BBCMG_Clip_Order SET Order_Status_Id=" + thisNextStatus + " WHERE Request_Number='" + thisRequestNumber + "' AND Order_Status_Id=" + thisStatus
                    'l_con.Execute l_strSQLUpdate
                    
                    thisAttachment = ""
                    thisAttachment = Replace(App.Path, "C:\Users\Tim.JCADOMAIN\source\repos\BBCMG_Email_Notifiers", "\\lat-vb6-dev-1") + "\attachments\BBCMG-GettyImages_Master_Portal_V1.pdf"
                    
                    If SendMailFromBBC(thisFrom, thisTo, thisCC, thisBCC, thisTitle, thisBody, l_rstGetData("error_notified"), thisAttachment) Then
'                    If SendMail(thisFrom, thisTo, thisCC, thisBCC, thisTitle, thisBody, l_rstGetData("error_notified"), thisAttachment) Then
                    
                        If Preview_Order_Id > 0 Then
                            l_strSentSQL = "INSERT INTO BBCMG_Notification_Sent(Notification_Id,Master_Order_Id,Preview_Order_Id,Sent_To,CC_To, BCC_To)"
                            l_strSentSQL = l_strSentSQL + " VALUES(" + CStr(Notification_Id) + "," + CStr(Clip_Order_Id) + "," + CStr(Preview_Order_Id) + ",'" + thisTo + "','" + thisCC + "','" + thisBCC + "')"
                        Else
                            l_strSentSQL = "INSERT INTO BBCMG_Notification_Sent(Notification_Id,Master_Order_Id,Sent_To,CC_To, BCC_To)"
                            l_strSentSQL = l_strSentSQL + " VALUES(" + CStr(Notification_Id) + "," + CStr(Clip_Order_Id) + ",'" + thisTo + "','" + thisCC + "','" + thisBCC + "')"
                        End If
                        
                        l_con.Execute l_strSentSQL
                        
                        If SetData("BBCMG_Notification_Queue", "Notification_Status", "Notification_Queue_Id", Notification_Queue_Id, 1) Then
                        
                        End If
                    Else
                        If SetData("BBCMG_Notification_Queue", "error_notified", "Notification_Queue_Id", Notification_Queue_Id, Format(Now, "YYYY-MM-DD HH:NN:SS")) Then
                        End If
                    End If
                    l_rstItemData.Close
                    
                End If
            
            Case 7
            '   Preview Portal Expiry
                 If Preview_Order_Id > 0 Then
                    thisFrom = "noreply@visualdatamedia.com"
                    
                    thisRequestNumber = GetData("BBCMG_Preview_Order", "Request_Number", "Preview_Order_Id", Preview_Order_Id)
                    thisCustomerName = GetData("BBCMG_Preview_Order", "Full_Name_Customer", "Preview_Order_Id", Preview_Order_Id)
                    thisDateReceived = GetData("BBCMG_Preview_Order", "CreatedOn", "Preview_Order_Id", Preview_Order_Id)
                    
                    'build message
                    thisSubmitter = GetData("BBCMG_Preview_Order", "Owner", "Preview_Order_Id", Preview_Order_Id)
                    thisTo = GetData("BBCMG_Sales_Lookup", "eMail", "Alt_Name", thisSubmitter)
                    If thisTo = "" Then thisTo = GetData("BBCMG_Sales_Lookup", "eMail", "Name", thisSubmitter)
                    thisCC = GetData("BBCMG_Preview_Order", "Created_By_Email", "Preview_Order_Id", Preview_Order_Id)
                    thisBCC = "thart@visualdatamedia.com,bbcmg-VIE@visualdatamedia.com"
                    
                    'test overrides
                    If bTestMode Then
                        thisBCC = ""
                        thisCC = "rrmedia.test2@gmail.com"
                        thisTo = "rrmedia.test1@gmail.com"
                    End If
                    
                    thisTitle = Replace(Notification_Title, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    thisBody = Replace(Notification_Body, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@WWOPERATIONS", "ww.mgpriority@bbc.com", 1, -1, vbTextCompare)

                    If InStr(thisCustomerName, ",") > 0 Then
                        unames = Split(thisCustomerName, ",")
                        thisName = unames(1)
                    Else
                        thisName = thisCustomerName
                    End If
                    
                    
                    thisBody = Replace(thisBody, "@@CUSTOMERNAME", thisCustomerName, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@POID", CStr(Preview_Order_Id), 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@DATERECEIVED", thisDateReceived, 1, -1, vbTextCompare)
           
                    thisBody = Replace(thisBody, "@@BBCMGIMAGE", thisLogo, 1, -1, vbTextCompare)
                    
                    If SendMail(thisFrom, thisTo, thisCC, thisBCC, thisTitle, thisBody, l_rstGetData("error_notified")) Then
                        l_strSentSQL = "INSERT INTO BBCMG_Notification_Sent(Notification_Id,Preview_Order_Id,Sent_To,CC_To, BCC_To)"
                        l_strSentSQL = l_strSentSQL + " VALUES(" + CStr(Notification_Id) + "," + CStr(Preview_Order_Id) + ",'" + thisTo + "','" + thisCC + "','" + thisBCC + "')"
                        l_con.Execute l_strSentSQL
                        
                        If SetData("BBCMG_Notification_Queue", "Notification_Status", "Notification_Queue_Id", Notification_Queue_Id, 1) Then
                        End If
                    Else
                        If SetData("BBCMG_Notification_Queue", "error_notified", "Notification_Queue_Id", Notification_Queue_Id, Format(Now, "YYYY-MM-DD HH:NN:SS")) Then
                        End If
                    End If
                
                End If
            
            Case 8
            '   Purge Lists available to be viewed.
                thisFrom = "noreply@visualdatamedia.com"
                
                'build message
                l_rstItemData.Open "SELECT * FROM trackernotification WHERE contractgroup = 'BBC' AND trackermessageID = 45", l_con, adOpenStatic, adLockReadOnly
                If l_rstItemData.RecordCount > 0 Then
                    thisTo = "thart@visualdatamedia.com"
                    l_rstItemData.MoveFirst
                    Do While Not l_rstItemData.EOF
                       thisTo = thisTo & "," & l_rstItemData("email")
                       l_rstItemData.MoveNext
                    Loop
                End If
                l_rstItemData.Close
                
                thisBCC = "thart@visualdatamedia.com,bbcmg-VIE@visualdatamedia.com,thart.VDMS@gmail.com"
                
                'test overrides
                If bTestMode Then
                    thisBCC = ""
                    thisCC = "rrmedia.test2@gmail.com"
                    thisTo = "rrmedia.test1@gmail.com"
                End If
                
                thisBody = Replace(Notification_Body, "@@WWOPERATIONS", "WWMG.Operations@bbc.com", 1, -1, vbTextCompare)
                thisBody = Replace(thisBody, "@@BBCMGIMAGE", thisLogo, 1, -1, vbTextCompare)
                
                thisTitle = Notification_Title
                
                If SendMail(thisFrom, thisTo, thisCC, thisBCC, thisTitle, thisBody, l_rstGetData("error_notified")) Then
                    If SetData("BBCMG_Notification_Queue", "Notification_Status", "Notification_Queue_Id", Notification_Queue_Id, 1) Then
                    End If
                Else
                    If SetData("BBCMG_Notification_Queue", "error_notified", "Notification_Queue_Id", Notification_Queue_Id, Format(Now, "YYYY-MM-DD HH:NN:SS")) Then
                    End If
                End If
            
            Case 9
            '   Masters Released Pending Recovery
                If Clip_Order_Id > 0 Then
                
                    thisFrom = "noreply@visualdatamedia.com"
                    
                    thisRequestNumber = GetData("BBCMG_Clip_Order", "Request_Number", "Clip_Order_Id", Clip_Order_Id)
                    thisPreviewRequestNumber = GetData("BBCMG_Clip_Order", "Preview_Request_Number", "Clip_Order_Id", Clip_Order_Id)
                    thisPreviewOrderId = GetData("BBCMG_Clip_Order", "Preview_Order_id", "Clip_Order_Id", Clip_Order_Id)
                    thisCustomerName = GetData("BBCMG_Preview_Order", "Full_Name_Customer", "Preview_Order_Id", thisPreviewOrderId)
                    thisDateReceived = GetData("BBCMG_Clip_Order", "CreatedOn", "Clip_Order_Id", Clip_Order_Id)
                    thisDeliveryMethod = GetData("BBCMG_Clip_Order", "Delivery_Method_BBC", "Clip_Order_Id", Clip_Order_Id)
                    thisFormat = GetData("BBCMG_Clip_Order", "Video_Format_Required", "Clip_Order_Id", Clip_Order_Id)
                    thisDuration = GetData("vw_BBCMG_Clip_Order_Summary", "Duration", "Request_Number", thisRequestNumber)
                    
                    If InStr(thisCustomerName, ",") > 0 Then
                        unames = Split(thisCustomerName, ",")
                        thisName = unames(1)
                    Else
                        thisName = thisCustomerName
                    End If
                    
                    If Not IsNull(GetData("BBCMG_Clip_Order", "Preview_Order_id", "Clip_Order_Id", Clip_Order_Id)) Then
                        Preview_Order_Id = GetData("BBCMG_Clip_Order", "Preview_Order_id", "Clip_Order_Id", Clip_Order_Id)
                    End If
                    
                    'build message
                    thisTo = GetData("BBCMG_Preview_Order", "Created_By_Email", "Preview_Order_Id", Preview_Order_Id)
                    thisSubmitter = GetData("BBCMG_Preview_Order", "Owner", "Preview_Order_Id", Preview_Order_Id)
                    thisCC = GetData("BBCMG_Sales_Lookup", "eMail", "Alt_Name", thisSubmitter)

                    thisBCC = "WWMG.Operations@bbc.com,thart@visualdatamedia.com,bbcmg-VIE@visualdatamedia.com"
                    
                    'test overrides
                    If bTestMode Then
                        thisBCC = ""
                        thisCC = "rrmedia.test2@gmail.com"
                        thisTo = "rrmedia.test1@gmail.com"
                    End If
                    
                    thisTitle = Replace(Notification_Title, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    thisBody = Replace(Notification_Body, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@CLIENTEMAIL", thisCustomerEmail, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@CUSTOMERNAME", thisCustomerName, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@TOTALLENGTH", thisDuration, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@VIDEOFORMAT", thisFormat, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@MASTERSLA", thisMasterSLA, 1, -1, vbTextCompare)
                                        
                    l_strItemSQL = "SELECT BBCMG_Preview_Order.Clip_ID as 'Clip_ID', BBCMG_Clip_Order.TX_Date, BBCMG_Clip_Order.Timecode_In, BBCMG_Clip_Order.Timecode_Out, " + _
                       "BBCMG_Clip_Order.Program_Id , BBCMG_Clip_Order.Program_Title, BBCMG_Clip_Order.Shot_Info, BBCMG_Clip_Order.Rights_Approval_Status " + _
                    "From BBCMG_Clip_Order  " + _
                               "Inner Join  " + _
                    "BBCMG_Preview_Order on " + _
                    "BBCMG_Preview_Order.Preview_Order_Id = BBCMG_Clip_Order.Preview_Order_Id" + vbCrLf
                    
                    l_strItemSQL = l_strItemSQL + " WHERE RTRIM(BBCMG_Clip_Order.Clip_Order_Id)='" + Trim(Clip_Order_Id) + "' AND BBCMG_Preview_Order.Order_Status_Id NOT IN(1,8)"
                        
                    l_rstItemData.Open l_strItemSQL, l_con, adOpenStatic, adLockReadOnly
                    
                    thisTable = "<table><tr><td><strong>Clip ID</strong></td>"
                    thisTable = thisTable + "<td><strong>TX Date</strong></td>"
                    thisTable = thisTable + "<td><strong>Time In</strong></td>"
                    thisTable = thisTable + "<td><strong>Time Out</strong></td>"
                    thisTable = thisTable + "<td><strong>BBC Programme ID</strong></td>"
                    thisTable = thisTable + "<td><strong>Programme Title</strong></td>"
                    thisTable = thisTable + "<td><strong>Shot Description</strong></td></tr>"
                    
                    On Local Error Resume Next
                    
                    If Not IsNull(l_rstItemData) Then
                        l_rstItemData.MoveFirst
                        Do While Not l_rstItemData.EOF
                            If Val(l_rstItemData("Rights_Approval_Status")) = Val(GetData("BBCMG_Rights_Status", "Rights_Status_Id", "Rights_Status", "Rights Approved")) Then
                                thisRow = "<tr>"
                                For i = 0 To l_rstItemData.Fields.Count - 1
                                    If l_rstItemData.Fields(i).Name = "Clip_ID" Then
                                        thisClipID = GetData("BBCMG_Preview_Order", "Clip_ID", "Preview_Order_Id", Preview_Order_Id)
                                        If Not IsNull(thisClipID) Then
                                            thisRow = thisRow + "<td>" + CStr(thisClipID) + "</td>"
                                        Else
                                            thisRow = thisRow + "<td>&nbsp;</td>"
                                        End If
                                    Else
                                        If Not IsNull(l_rstItemData.Fields(i).Value) Then
                                            thisRow = thisRow + "<td>" + CStr(l_rstItemData.Fields(i).Value) + "</td>"
                                        Else
                                            thisRow = thisRow + "<td>&nbsp;</td>"
                                        End If
                                    End If
                                Next
                                thisRow = thisRow + "</tr>"
                                thisTable = thisTable + thisRow
                            End If
                            l_rstItemData.MoveNext
                        Loop
                    End If
                    
                    thisTable = thisTable + "</table>"
                    
                    thisBody = Replace(thisBody, "@@CLIPLISTING", thisTable, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@GETTYLOGO", thisLogo, 1, -1, vbTextCompare)
                    
                    If SendMail(thisFrom, thisTo, thisCC, thisBCC, thisTitle, thisBody, l_rstGetData("error_notified")) Then
                    
                        If Preview_Order_Id > 0 Then
                            l_strSentSQL = "INSERT INTO BBCMG_Notification_Sent(Notification_Id,Master_Order_Id,Preview_Order_Id,Sent_To,CC_To, BCC_To)"
                            l_strSentSQL = l_strSentSQL + " VALUES(" + CStr(Notification_Id) + "," + CStr(Clip_Order_Id) + "," + CStr(Preview_Order_Id) + ",'" + thisTo + "','" + thisCC + "','" + thisBCC + "')"
                        Else
                            l_strSentSQL = "INSERT INTO BBCMG_Notification_Sent(Notification_Id,Master_Order_Id,Sent_To,CC_To, BCC_To)"
                            l_strSentSQL = l_strSentSQL + " VALUES(" + CStr(Notification_Id) + "," + CStr(Clip_Order_Id) + ",'" + thisTo + "','" + thisCC + "','" + thisBCC + "')"
                        End If
                        
                        l_con.Execute l_strSentSQL
                        
                        If SetData("BBCMG_Notification_Queue", "Notification_Status", "Notification_Queue_Id", Notification_Queue_Id, 1) Then
                        End If
                    Else
                        If SetData("BBCMG_Notification_Queue", "error_notified", "Notification_Queue_Id", Notification_Queue_Id, Format(Now, "YYYY-MM-DD HH:NN:SS")) Then
                        End If
                    End If
                    
                End If
            
            Case 11
            '   Masters Released and Recovered
                If Clip_Order_Id > 0 Then
                
                    thisFrom = "noreply@visualdatamedia.com"
                    
                    thisRequestNumber = GetData("BBCMG_Clip_Order", "Request_Number", "Clip_Order_Id", Clip_Order_Id)
                    thisPreviewRequestNumber = GetData("BBCMG_Clip_Order", "Preview_Request_Number", "Clip_Order_Id", Clip_Order_Id)
                    thisPreviewOrderId = GetData("BBCMG_Clip_Order", "Preview_Order_id", "Clip_Order_Id", Clip_Order_Id)
                    thisCustomerName = GetData("BBCMG_Preview_Order", "Full_Name_Customer", "Preview_Order_Id", thisPreviewOrderId)
                    thisDateReceived = GetData("BBCMG_Clip_Order", "CreatedOn", "Clip_Order_Id", Clip_Order_Id)
                    thisDeliveryMethod = GetData("BBCMG_Clip_Order", "Delivery_Method_BBC", "Clip_Order_Id", Clip_Order_Id)
                    thisFormat = GetData("BBCMG_Clip_Order", "Video_Format_Required", "Clip_Order_Id", Clip_Order_Id)
                    thisDuration = GetData("vw_BBCMG_Clip_Order_Summary", "Duration", "Request_Number", thisRequestNumber)
                    
                    If InStr(thisCustomerName, ",") > 0 Then
                        unames = Split(thisCustomerName, ",")
                        thisName = unames(1)
                    Else
                        thisName = thisCustomerName
                    End If
                    
                    If Not IsNull(GetData("BBCMG_Clip_Order", "Preview_Order_id", "Clip_Order_Id", Clip_Order_Id)) Then
                        Preview_Order_Id = GetData("BBCMG_Clip_Order", "Preview_Order_id", "Clip_Order_Id", Clip_Order_Id)
                    End If
                    
                    'build message
                    thisTo = GetData("BBCMG_Preview_Order", "Created_By_Email", "Preview_Order_Id", Preview_Order_Id)
                    thisSubmitter = GetData("BBCMG_Preview_Order", "Owner", "Preview_Order_Id", Preview_Order_Id)
                    thisCC = GetData("BBCMG_Sales_Lookup", "eMail", "Alt_Name", thisSubmitter)

                    thisBCC = "WWMG.Operations@bbc.com,thart@visualdatamedia.com,bbcmg-VIE@visualdatamedia.com"
                    
                    'test overrides
                    If bTestMode Then
                        thisBCC = ""
                        thisCC = "rrmedia.test2@gmail.com"
                        thisTo = "rrmedia.test1@gmail.com"
                    End If
                    
                    thisTitle = Replace(Notification_Title, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    thisBody = Replace(Notification_Body, "@@REQUESTNUMBER", thisRequestNumber, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@CLIENTEMAIL", thisCustomerEmail, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@CUSTOMERNAME", thisCustomerName, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@TOTALLENGTH", thisDuration, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@VIDEOFORMAT", thisFormat, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@MASTERSLA", thisMasterSLA, 1, -1, vbTextCompare)
                                        
                    l_strItemSQL = "SELECT BBCMG_Preview_Order.Clip_ID as 'Clip_ID', BBCMG_Clip_Order.TX_Date, BBCMG_Clip_Order.Timecode_In, BBCMG_Clip_Order.Timecode_Out, " + _
                       "BBCMG_Clip_Order.Program_Id , BBCMG_Clip_Order.Program_Title, BBCMG_Clip_Order.Shot_Info, BBCMG_Clip_Order.Rights_Approval_Status " + _
                    "From BBCMG_Clip_Order  " + _
                               "Inner Join  " + _
                    "BBCMG_Preview_Order on " + _
                    "BBCMG_Preview_Order.Preview_Order_Id = BBCMG_Clip_Order.Preview_Order_Id" + vbCrLf
                    
                    l_strItemSQL = l_strItemSQL + " WHERE RTRIM(BBCMG_Clip_Order.Clip_Order_Id)='" + Trim(Clip_Order_Id) + "' AND BBCMG_Preview_Order.Order_Status_Id NOT IN(1,8)"
                        
                    l_rstItemData.Open l_strItemSQL, l_con, adOpenStatic, adLockReadOnly
                    
                    thisTable = "<table><tr><td><strong>Clip ID</strong></td>"
                    thisTable = thisTable + "<td><strong>TX Date</strong></td>"
                    thisTable = thisTable + "<td><strong>Time In</strong></td>"
                    thisTable = thisTable + "<td><strong>Time Out</strong></td>"
                    thisTable = thisTable + "<td><strong>BBC Programme ID</strong></td>"
                    thisTable = thisTable + "<td><strong>Programme Title</strong></td>"
                    thisTable = thisTable + "<td><strong>Shot Description</strong></td></tr>"
                    
                    On Local Error Resume Next
                    
                    If Not IsNull(l_rstItemData) Then
                        l_rstItemData.MoveFirst
                        Do While Not l_rstItemData.EOF
                            If Val(l_rstItemData("Rights_Approval_Status")) = Val(GetData("BBCMG_Rights_Status", "Rights_Status_Id", "Rights_Status", "Rights Approved")) Then
                                thisRow = "<tr>"
                                For i = 0 To l_rstItemData.Fields.Count - 1
                                    If l_rstItemData.Fields(i).Name = "Clip_ID" Then
                                        thisClipID = GetData("BBCMG_Preview_Order", "Clip_ID", "Preview_Order_Id", Preview_Order_Id)
                                        If Not IsNull(thisClipID) Then
                                            thisRow = thisRow + "<td>" + CStr(thisClipID) + "</td>"
                                        Else
                                            thisRow = thisRow + "<td>&nbsp;</td>"
                                        End If
                                    Else
                                        If Not IsNull(l_rstItemData.Fields(i).Value) Then
                                            thisRow = thisRow + "<td>" + CStr(l_rstItemData.Fields(i).Value) + "</td>"
                                        Else
                                            thisRow = thisRow + "<td>&nbsp;</td>"
                                        End If
                                    End If
                                Next
                                thisRow = thisRow + "</tr>"
                                thisTable = thisTable + thisRow
                            End If
                            l_rstItemData.MoveNext
                        Loop
                    End If
                    
                    thisTable = thisTable + "</table>"
                    
                    thisBody = Replace(thisBody, "@@CLIPLISTING", thisTable, 1, -1, vbTextCompare)
                    thisBody = Replace(thisBody, "@@GETTYLOGO", thisLogo, 1, -1, vbTextCompare)
                    
                    If SendMail(thisFrom, thisTo, thisCC, thisBCC, thisTitle, thisBody, l_rstGetData("error_notified")) Then
                    
                        If Preview_Order_Id > 0 Then
                            l_strSentSQL = "INSERT INTO BBCMG_Notification_Sent(Notification_Id,Master_Order_Id,Preview_Order_Id,Sent_To,CC_To, BCC_To)"
                            l_strSentSQL = l_strSentSQL + " VALUES(" + CStr(Notification_Id) + "," + CStr(Clip_Order_Id) + "," + CStr(Preview_Order_Id) + ",'" + thisTo + "','" + thisCC + "','" + thisBCC + "')"
                        Else
                            l_strSentSQL = "INSERT INTO BBCMG_Notification_Sent(Notification_Id,Master_Order_Id,Sent_To,CC_To, BCC_To)"
                            l_strSentSQL = l_strSentSQL + " VALUES(" + CStr(Notification_Id) + "," + CStr(Clip_Order_Id) + ",'" + thisTo + "','" + thisCC + "','" + thisBCC + "')"
                        End If
                        
                        l_con.Execute l_strSentSQL
                        
                        If SetData("BBCMG_Notification_Queue", "Notification_Status", "Notification_Queue_Id", Notification_Queue_Id, 1) Then
                        End If
                    Else
                        If SetData("BBCMG_Notification_Queue", "error_notified", "Notification_Queue_Id", Notification_Queue_Id, Format(Now, "YYYY-MM-DD HH:NN:SS")) Then
                        End If
                    End If
                    
                End If
                        
            Case 12
            '   Order for a tape already here
                If Preview_Order_Id > 0 Then
                    
                    thisRequestNumber = GetData("BBCMG_Preview_order", "Request_Number", "Preview_Order_Id", Preview_Order_Id)
                    thisBarcode = GetData("BBCMG_Preview_Order", "Tape_Number", "Preview_Order_Id", Preview_Order_Id)
                    thisFrom = "noreply@visualdatamedia.com"
                    thisBCC = "thart@visualdatamedia.com"
                    thisCC = ""
                    thisTitle = "BBCMG Preview Order received for tape already here"
                    thisBody = "An order has been recieved for a Tape already here:" & vbCrLf & "Request No: " & thisRequestNumber & vbCrLf & "Tape Barcode: " & thisBarcode & vbCrLf
                    thisBody = thisBody & "Loaction: " & GetData("library", "location", "barcode", thisBarcode) & vbCrLf
                    thisBody = thisBody & "Shelf: " & GetData("library", "shelf", "barcode", thisBarcode) & vbCrLf
                    
                    Set l_rstCirculation = New ADODB.Recordset
                    l_strSQL = "SELECT * FROM trackernotification WHERE trackermessageID = 48"
                    l_rstCirculation.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly
                    If l_rstCirculation.RecordCount > 0 Then
                        l_rstCirculation.MoveFirst
                        Do While Not l_rstCirculation.EOF
                            SendMail thisFrom, l_rstCirculation("email"), thisCC, thisBCC, thisTitle, thisBody, l_rstGetData("error_notified")
                            l_rstCirculation.MoveNext
                        Loop
                    End If
                    l_rstCirculation.Close
                    Set l_rstCirculation = Nothing
                
                    If SetData("BBCMG_Notification_Queue", "Notification_Status", "Notification_Queue_Id", Notification_Queue_Id, 1) Then
                    End If
                
                End If
            Case Else
            
            End Select
            
            If Count = 1 Then
                Exit Do
            End If
        Loop
    End If

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing

    GetMailJobs = i

        Exit Function

GetData_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in modMailNotify.GetMailJobs " & _
               "at line " & Erl
        Resume Next


End Function

Public Function SendMail(MailFrom As String, _
    MailTo As String, _
    MailCC As String, _
    MailBCC As String, _
    Subject As String, _
    Message As String, _
    Error_Notified As Variant, _
    Optional Attachment1 As String = "", _
    Optional Attachment2 As String = "") As Boolean

    On Error GoTo PROC_ERR
    
    Dim SQL As String

    SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyHTML, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
    SQL = SQL & "'" & QuoteSanitise(MailTo) & "', "
    SQL = SQL & "'" & QuoteSanitise(MailCC) & "', "
    SQL = SQL & "'" & QuoteSanitise(MailBCC) & "', "
    SQL = SQL & "'" & QuoteSanitise(Subject) & "', "
    SQL = SQL & "'" & QuoteSanitise(Message) & "', "
    
    If Attachment1 <> "" Then
        SQL = SQL & "'" & QuoteSanitise(Attachment1)
    End If
    
    If Attachment2 <> "" Then
        If Attachment1 <> "" Then
            SQL = SQL & ","
        Else
            SQL = SQL & "'"
        End If
        SQL = SQL & QuoteSanitise(Attachment2)
    End If
    If Attachment1 <> "" Or Attachment2 <> "" Then
        SQL = SQL & "', "
    Else
        SQL = SQL & "Null, "
    End If
    
    SQL = SQL & "'" & QuoteSanitise(MailFrom) & "', "
    SQL = SQL & "'BBCMG', 'BBCMG', 1);"
    
    Dim cnn As New ADODB.Connection
    Debug.Print SQL
    cnn.Open g_strConnection
    cnn.Execute SQL
    cnn.Close
    Set cnn = Nothing

    SendMail = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    App.LogEvent Err.Description
    SendMail = False
    Resume PROC_EXIT
End Function

Public Function SendMailOLD(MailFrom As String, _
    MailTo As String, _
    MailCC As String, _
    MailBCC As String, _
    Subject As String, _
    Message As String, _
    Error_Notified As Variant, _
    Optional Attachment1 As String = "", _
    Optional Attachment2 As String = "") As Boolean

    On Error GoTo PROC_ERR
    
    Dim ObjSendMail
    Set ObjSendMail = CreateObject("CDO.Message")

    'This section provides the configuration information for the remote SMTP server.

    With ObjSendMail.Configuration.Fields
    .Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2  'Send the message using the network (SMTP over the network).
    .Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = g_strSMTPServer
    .Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
    .Item("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = False  'Use SSL for the connection (True or False)
    .Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

    ' If your server requires outgoing authentication uncomment the lines below and use a valid email address and password.
    .Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1  'basic (clear-text) authentication
    .Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = g_strSMTPUserName
    .Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = g_strSMTPPassword

    .Update
    End With

    'End remote SMTP server configuration section==

    ObjSendMail.To = MailTo
    ObjSendMail.Subject = Subject
    ObjSendMail.From = MailFrom
    ObjSendMail.CC = MailCC
    ObjSendMail.BCC = MailBCC
    
    If Attachment1 <> "" Then
        ObjSendMail.addattachment Attachment1
    End If
    
    If Attachment2 <> "" Then
        ObjSendMail.addattachment Attachment1
    End If
    
    ' we are sending a html email.. simply switch the comments around to send a text email instead
    ObjSendMail.htmlbody = Message
    'ObjSendMail.TextBody = Message

    ObjSendMail.send

    Set ObjSendMail = Nothing
    
    SendMailOLD = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    App.LogEvent Err.Description
    If IsEmpty(Error_Notified) Or IsNull(Error_Notified) Then
        ObjSendMail.To = "thart@visualdatamedia.com,bbcmg-VIE@visualdatamedia.com"
        ObjSendMail.Subject = "BBCMG Mail Queue message failed to send - this will be clogging the queue"
        ObjSendMail.textbody = "Original Subject line: " & Subject
        ObjSendMail.htmlbody = ""
        ObjSendMail.CC = ""
        ObjSendMail.BCC = ""
        ObjSendMail.addattachment = ""
        ObjSendMail.send
    End If
    SendMailOLD = False
    Resume PROC_EXIT
End Function

Public Function SendMailFromBBC(MailFrom As String, _
    MailTo As String, _
    MailCC As String, _
    MailBCC As String, _
    Subject As String, _
    Message As String, _
    Error_Notified As Variant, _
    Optional Attachment1 As String = "", _
    Optional Attachment2 As String = "") As Boolean

    On Error GoTo PROC_ERR
    
    Dim ObjSendMail
    Set ObjSendMail = CreateObject("CDO.Message")

    'This section provides the configuration information for the remote SMTP server.

    With ObjSendMail.Configuration.Fields
    .Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2  'Send the message using the network (SMTP over the network).
    .Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = g_strSMTPServerBBC
    .Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
    .Item("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = False  'Use SSL for the connection (True or False)
    .Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

    ' If your server requires outgoing authentication uncomment the lines below and use a valid email address and password.
    .Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1  'basic (clear-text) authentication
    .Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = g_strSMTPUserName
    .Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = g_strSMTPPassword

    .Update
    End With

    'End remote SMTP server configuration section==

    ObjSendMail.To = MailTo
    ObjSendMail.Subject = Subject
    ObjSendMail.From = MailFrom
    ObjSendMail.CC = MailCC
    ObjSendMail.BCC = MailBCC
    
    If Attachment1 <> "" Then
        ObjSendMail.addattachment Attachment1
    End If
    
    If Attachment2 <> "" Then
        ObjSendMail.addattachment Attachment2
    End If
    
    ' we are sending a html email.. simply switch the comments around to send a text email instead
    ObjSendMail.htmlbody = Message
    'ObjSendMail.TextBody = Message

    ObjSendMail.send

    Set ObjSendMail = Nothing
    
    SendMailFromBBC = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    App.LogEvent Err.Description
    If IsEmpty(Error_Notified) Or IsNull(Error_Notified) Then
        ObjSendMail.To = "thart@visualdatamedia.com,bbcmg-VIE@visualdatamedia.com"
        ObjSendMail.Subject = "BBCMG Mail Queue message failed to send - this will be clogging the queue"
        ObjSendMail.textbody = "Original Subject line: " & Subject
        ObjSendMail.htmlbody = ""
        ObjSendMail.CC = ""
        ObjSendMail.BCC = ""
        ObjSendMail.addattachment = ""
        ObjSendMail.send
    End If
    SendMailFromBBC = False
    Resume PROC_EXIT
End Function

Public Function FormatSQLDate(lDate As Variant)
    
    On Error GoTo PROC_ERR

    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    App.LogEvent Err.Description
    Resume PROC_EXIT
    
End Function

Function QuoteSanitise(lp_strText) As String
On Error GoTo QuoteSanitise_Err

        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Long

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
    On Error GoTo GetData_Err


    Dim l_strSQL As String
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = UTF8_Decode(l_rstGetData(lp_strFieldToReturn))
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function

Function GetDataSQL(lp_strSQL As String) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strConnection
    l_rstGetData.Open lp_strSQL, l_con, adOpenStatic, adLockReadOnly

    If l_rstGetData.RecordCount > 0 Then
        If Not IsNull(l_rstGetData.Fields(0)) Then
            l_rstGetData.MoveFirst
            GetDataSQL = l_rstGetData.Fields(0)
            GoTo Proc_CloseDB
        
        End If
    Else
        GoTo Proc_CloseDB
    End If

    Select Case l_rstGetData.Fields(0).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetDataSQL = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetDataSQL = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetDataSQL = 0
    Case Else
        GetDataSQL = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function

Function SetData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant

Dim l_strSQL As String, l_UpdateValue As String, c As ADODB.Connection

If UCase(lp_varValue) = "NULL" Or lp_varValue = "" Then
    l_UpdateValue = "NULL"
Else
    l_UpdateValue = "'" & lp_varValue & "'"
End If

l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & lp_varCriterea & "'"

Set c = New ADODB.Connection
c.Open g_strConnection

c.Execute l_strSQL
c.Close
Set c = Nothing

End Function

Public Function HasExcel() As Boolean

Dim ExcelObj As Object
Dim b As Boolean

    On Error Resume Next
    Set ExcelObj = CreateObject("Excel.Application")
    On Error GoTo 0
    
    If ExcelObj Is Nothing Then
        b = False
        Else
        b = True
    End If
    
    HasExcel = b

End Function

