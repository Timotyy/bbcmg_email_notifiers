VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMailNotify 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "BBC MG Excel Order Importer"
   ClientHeight    =   7500
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   10500
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   OLEDropMode     =   1  'Manual
   ScaleHeight     =   500
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   700
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnLoad 
      Caption         =   "Load"
      Height          =   495
      Left            =   2520
      TabIndex        =   15
      Top             =   6720
      Width           =   1335
   End
   Begin VB.TextBox emailCustomer 
      Height          =   405
      Left            =   6360
      TabIndex        =   1
      Top             =   120
      Width           =   3975
   End
   Begin VB.CommandButton btnImport 
      Caption         =   "Run Import"
      Height          =   495
      Index           =   0
      Left            =   360
      TabIndex        =   9
      Top             =   6720
      Width           =   1815
   End
   Begin VB.TextBox txtMail2 
      Height          =   405
      Left            =   6360
      TabIndex        =   3
      Top             =   1080
      Width           =   3975
   End
   Begin VB.TextBox txtMail 
      Height          =   405
      Left            =   6360
      TabIndex        =   2
      Top             =   600
      Width           =   3975
   End
   Begin VB.CommandButton btn 
      Appearance      =   0  'Flat
      Caption         =   "Clip Orders"
      Height          =   495
      Index           =   1
      Left            =   2160
      TabIndex        =   7
      Top             =   1200
      Width           =   1815
   End
   Begin VB.CommandButton btn 
      Appearance      =   0  'Flat
      Caption         =   "Preview Orders"
      Height          =   495
      Index           =   0
      Left            =   120
      TabIndex        =   6
      Top             =   1200
      Width           =   1815
   End
   Begin VB.Frame frPreviewOrders 
      Caption         =   "Preview Orders"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Left            =   120
      TabIndex        =   4
      Top             =   1800
      Width           =   10215
      Begin MSComctlLib.ListView lstPreview 
         Height          =   3975
         Left            =   240
         TabIndex        =   5
         Top             =   360
         Width           =   9735
         _ExtentX        =   17171
         _ExtentY        =   7011
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         OLEDropMode     =   1
         Checkboxes      =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   0
         OLEDropMode     =   1
         NumItems        =   0
      End
      Begin MSComctlLib.ListView LstClip 
         Height          =   3975
         Left            =   240
         TabIndex        =   8
         Top             =   360
         Width           =   9735
         _ExtentX        =   17171
         _ExtentY        =   7011
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         OLEDropMode     =   1
         Checkboxes      =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   0
         OLEDropMode     =   1
         NumItems        =   0
      End
   End
   Begin VB.Label lblType 
      Caption         =   "Drag n Drop a File to Process"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   495
      Left            =   120
      TabIndex        =   14
      Top             =   600
      Width           =   4335
   End
   Begin VB.Label lblTitle 
      Caption         =   "BBCMG Order Importer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   13
      Top             =   120
      Width           =   4335
   End
   Begin VB.Label lbl 
      Caption         =   "Customer eMail:"
      Height          =   375
      Index           =   2
      Left            =   4680
      TabIndex        =   12
      Top             =   120
      Width           =   1575
   End
   Begin VB.Label lbl 
      Caption         =   "Secondary eMail:"
      Height          =   375
      Index           =   1
      Left            =   4680
      TabIndex        =   11
      Top             =   1080
      Width           =   1575
   End
   Begin VB.Label lbl 
      Caption         =   "Primary eMail:"
      Height          =   375
      Index           =   0
      Left            =   4680
      TabIndex        =   10
      Top             =   600
      Width           =   1575
   End
   Begin VB.Label lblDrop 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Drop your Excel Files here"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   735
      Left            =   4320
      OLEDropMode     =   1  'Manual
      TabIndex        =   0
      Top             =   6600
      Width           =   6015
   End
End
Attribute VB_Name = "frmMailNotify"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Order_Type As SpreadsheetType

Private Sub btn_Click(Index As Integer)

    SwitchListing (Index)
    
End Sub

Private Sub btnImport_Click(Index As Integer)

    Dim item As Integer
    Dim litem As ListItem
    Dim sPrimary As String
    Dim sSecondary As String
    Dim sCustomer As String
    
    sPrimary = Trim(txtMail.Text)
    sSecondary = Trim(txtMail2.Text)
    sCustomer = Trim(emailCustomer.Text)
    
    If Index = 0 Then
        For item = 1 To lstPreview.ListItems.Count
            Set litem = lstPreview.ListItems.item(item)
            If litem.Checked Then
                'add a record for each row
                'and overried the email addresses
                If PreviewOrder_Add(litem, sPrimary, sSecondary, sCustomer) Then
                    '
    
                Else
                    'failed to add
                End If
                
            End If
        Next
    End If
    
    lstPreview.ListItems.Clear
    emailCustomer.Text = ""
    txtMail.Text = ""
    txtMail2.Text = ""
    lblType.Caption = "Drag n Drop a File to Process"
    MsgBox "Preview Order(s) were added to the system"
                    
End Sub

Private Sub btnLoad_Click()

    Dim fname As Variant
    
    fname = App.Path + "\R-1134150-K8G1.xlsx"

    If Verify_Import(fname, lstPreview, LstClip, emailCustomer, txtMail, txtMail2, Order_Type) Then
        Select Case Order_Type
        Case 0
            lblType.Caption = "Preview Order Detected"
            lstPreview.Visible = True
            LstClip.Visible = False
            LstClip.ListItems.Clear
        Case 1
            lblType.Caption = "Clip Order Detected"
            lstPreview.Visible = False
            LstClip.Visible = True
            lstPreview.ListItems.Clear
        End Select
    End If

End Sub

Private Sub Form_Unload(Cancel As Integer)

    End
    
End Sub

Public Sub SwitchListing(Index As Integer)
    Select Case Index
    Case 0
        lstPreview.Visible = True
        LstClip.Visible = False
        frPreviewOrders.Caption = "Preview Orders"
    Case 1
        lstPreview.Visible = False
        LstClip.Visible = True
        frPreviewOrders.Caption = "Master Clip Orders"
    End Select
End Sub


Private Sub lblDrop_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    Dim txt As String
    Dim fname As Variant
    
    emailCustomer.Text = ""
    txtMail.Text = ""
    txtMail2.Text = ""
    lblType.Caption = "Drag n Drop a File to Process"

    For Each fname In Data.Files
        If Verify_Import(fname, lstPreview, LstClip, emailCustomer, txtMail, txtMail2, Order_Type) Then
            Select Case Order_Type
            Case 0
                lblType.Caption = "Preview Order Detected"
                lstPreview.Visible = True
                LstClip.Visible = False
                LstClip.ListItems.Clear
            Case 1
                lblType.Caption = "Clip Order Detected"
                lstPreview.Visible = False
                LstClip.Visible = True
                lstPreview.ListItems.Clear
            End Select
        End If
    Next fname
    'MsgBox txt

    ' Indicate we did nothing with the files.
    Effect = vbDropEffectNone
    
End Sub

Private Sub lblDrop_OLEDragOver(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single, State As Integer)
    If Data.GetFormat(vbCFFiles) = True Then
        If (GetAttr(Data.Files.item(1)) And vbDirectory) Then ' don't accept folders
            Effect = vbDropEffectNone
        Else
            Effect = vbDropEffectCopy
        End If
    End If
End Sub

