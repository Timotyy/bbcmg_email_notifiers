Attribute VB_Name = "modUTF8Conv"
Option Explicit


Public Declare Function WideCharToMultiByte Lib "kernel32" (ByVal CodePage As Long, ByVal dwFlags As Long, ByVal lpWideCharStr As Long, ByVal cchWideChar As Long, ByRef lpMultiByteStr As Any, ByVal cchMultiByte As Long, ByVal lpDefaultChar As String, ByVal lpUsedDefaultChar As Long) As Long
Public Declare Function MultiByteToWideChar Lib "kernel32" (ByVal CodePage As Long, ByVal dwFlags As Long, ByRef lpMultiByteStr As Any, ByVal cchMultiByte As Long, ByVal lpWideCharStr As Long, ByVal cchWideChar As Long) As Long
Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (lpvDest As Any, lpvSource As Any, ByVal cbCopy As Long)

Public Const CP_UTF8 = 65001

'Purpose:Convert Utf8 to Unicode
Public Function UTF8_Decode(ByVal sUTF8 As String) As String

   Dim lngUtf8Size      As Long
   Dim strBuffer        As String
   Dim lngBufferSize    As Long
   Dim lngResult        As Long
   Dim bytUtf8()        As Byte
   Dim n                As Long

   If LenB(sUTF8) = 0 Then Exit Function

    On Error GoTo EndFunction
    bytUtf8 = StrConv(sUTF8, vbFromUnicode)
    lngUtf8Size = UBound(bytUtf8) + 1

    On Error GoTo 0
    'Set buffer for longest possible string i.e. each byte is
    'ANSI, thus 1 unicode(2 bytes)for every utf-8 character.

    lngBufferSize = lngUtf8Size * 2
    strBuffer = String$(lngBufferSize, vbNullChar)

    'Translate using code page 65001(UTF-8)
    lngResult = MultiByteToWideChar(CP_UTF8, 0, bytUtf8(0), lngUtf8Size, StrPtr(strBuffer), lngBufferSize)

    'Trim result to actual length
    If lngResult Then
       UTF8_Decode = Left$(strBuffer, lngResult)
    End If

EndFunction:

End Function

'Purpose:Convert Unicode string to UTF-8.
Public Function UTF8_Encode(ByVal strUnicode As String, Optional ByVal bHTML As Boolean = True) As String

   Dim i                As Long
   Dim TLen             As Long
   Dim lPtr             As Long
   Dim UTF16            As Long
   Dim UTF8_EncodeLong  As String

   TLen = Len(strUnicode)

   If TLen = 0 Then Exit Function

    Dim lngBufferSize    As Long
    Dim lngResult        As Long
    Dim bytUtf8()        As Byte
    
    'Set buffer for longest possible string.
    lngBufferSize = TLen * 3 + 1
    ReDim bytUtf8(lngBufferSize - 1)
    
    'Translate using code page 65001(UTF-8).
    lngResult = WideCharToMultiByte(CP_UTF8, 0, StrPtr(strUnicode), TLen, bytUtf8(0), lngBufferSize, vbNullString, 0)
    
    'Trim result to actual length.
    
    If lngResult Then
    
       lngResult = lngResult - 1
       ReDim Preserve bytUtf8(lngResult)
    
       'CopyMemory StrPtr(UTF8_Encode), bytUtf8(0&), lngResult
       UTF8_Encode = StrConv(bytUtf8, vbUnicode)
    
       ' For i = 0 To lngResult
       '    UTF8_Encode = UTF8_Encode & Chr$(bytUtf8(i))
       ' Next
    
    End If
    
   'Substitute vbCrLf with HTML line breaks if requested.
   If bHTML Then

      UTF8_Encode = Replace$(UTF8_Encode, vbCrLf, "")

   End If

End Function

